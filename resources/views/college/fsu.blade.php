@extends('layouts.app')

@section('title','Florida State University')

@section('content')
    <h1> Florida State University </h1>
    <hr>

    <p> Florida State University one of three higher level learning institutions in Tallahassee. There are always a lot of activities going on on campus during the day and night.
        Whether you live on or off campus, there is always something that is going on for students to enjoy. </p>


    <div class="col-md-12">
        @include('table.table')
    </div>


@endsection

@section('javascript')
    @include('layouts.pagination', ['dataType' => 'fsuData'])
@endsection

@section('extend.footer')
    <p class="small">Map data is provided by Google and DigitalGlobe<br />
        Map data: <i class="fa fa-copyright"></i>{{date("Y")}} Google</p>
@endsection