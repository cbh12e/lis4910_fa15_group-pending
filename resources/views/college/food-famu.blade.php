@extends('layouts.app')
@section('title','Florida Agricultural and Mechanical University')

@section('custom-styles')
        <!-- FAMU Custom Styles -->
<link type="text/css" rel="stylesheet" href="{{cdn('css/famu-custom-styles.css')}}">
@overwrite

@section('content')
    <div class="col-md-12">
        <h1>Restaurants located near FAMU</h1>
        <hr>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="lead">
                There are many restaurants in town that are located near Florida Agricultural and Mechanical University.
                Here is an exclusive list of all restaurants located within a mile radius of the university.
            </p>
        </div>
    </div>



    <div class="row">
        <div class="col-md-10">

            {{-- Located in the table directory resources/views/table/restaurant.blade.php --}}
            @include('table.restaurant')

        </div>

        {{-- Planning on placing Google Adsense here to provide some income to maintain the website.--}}
        <div class="col-md-2">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- FAMU Food -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:160px;height:250px"
                 data-ad-client="ca-pub-8386994709092971"
                 data-ad-slot="5919084644"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- FAMU Responsive Text -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-8386994709092971"
                 data-ad-slot="2826017445"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
@endsection

@section('javascript')
    @include('layouts.pagination', ['dataType' => 'famu/restaurantData'])
@endsection
@endsection