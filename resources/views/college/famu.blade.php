@extends('layouts.app')

@section('title','Florida Agricultural and Mechanical University')

@section('custom-styles')

    <!-- FAMU Custom Styles -->
    <link type="text/css" rel="stylesheet" href="{{cdn('css/famu-custom-styles.css')}}">

@overwrite

@section('content')
<h1> Florida Agricultural and Mechanical University (FAMU) </h1>
<a href="{{url('famu/restaurants')}}" class="btn btn-primary"> Restaurants</a>

<hr>

    <p> Florida Agricultural and Mechanical University better known as FAMU is the only surviving historically public black university in Florida. </p>
<br />


    @include('table.table')

@endsection

@section('javascript')
    @include('layouts.pagination', ['dataType' => 'famuData'])
@endsection

@section('extend.footer')
    <p class="small">Map data is provided by Google and DigitalGlobe<br />
        Map data: <i class="fa fa-copyright"></i>{{date("Y")}} Google</p>
@endsection