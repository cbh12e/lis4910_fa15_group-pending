@extends('layouts.app')
@section('title','Parks')
@section('content')

    @include('adsense.header')

<div class="row">
    <div class="col-md-12">
        {{-- Paragraph Text --}}
        <p class="lead"> Tallahassee has a wide variety of parks and trail system that can be visited. There is also an national forest, multiple state parks and a National Wildlife Refuge located within a hour's distance to Tallahassee.  </p>
    </div>
</div>
<div class="col-md-12">
        {{-- Thumbnail data Blade - This file is located in the layouts subdirectory in the table directory. This file is called list.blade.php --}}
        @include('table.park')
</div>

    @include('adsense.bottom1')

@endsection
@section('javascript')
    @include('layouts.pagination', ['dataType' => 'parkData'])
@endsection
@section('extend.footer')
    <p class="small">Map data is provided by Google and DigitalGlobe<br />
        Map data: <i class="fa fa-copyright"></i>{{date("Y")}} Google</p>
@endsection