@extends('layouts.app')
@section('title','Nightlife')
@section('content')

    @include('adsense.header')

    <div class="row">
        <div class="col-md-12">
            <p class="lead">
                Tallahassee has a lot of night entertainment available. Here is a list of many places that are available for nightly entertainment in town. </p>
        </div>
    </div>

    <div class="row">
        {{-- Located in the table directory resources/views/table/bars.blade.php --}}
        @include('table.table')
    </div>

    @include('adsense.bottom')

@endsection

@section('javascript')
    @include('layouts.pagination', ['dataType' => 'nightData'])
@endsection
