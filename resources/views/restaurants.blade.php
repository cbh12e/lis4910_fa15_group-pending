@extends('layouts.app')
@section('title','Restaurants')
@section('content')

    @include('adsense.header')

<div class="row">
    <div class="col-md-12">
        <p class="lead">
            As a college town, Tallahassee has a diverse choice of food options that will appeal to everyone. Here are a list of some of the restaurants to choose from.
        </p>
    </div>
    </div>



<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-10 col-sm-12 col-xs-12">

        {{-- Located in the table directory resources/views/table/restaurant.blade.php --}}
        @include('table.restaurant')

    </div>

    @include('adsense.sidebar')

    </div>

    @include('adsense.bottom1')

@endsection

@section('javascript')
    @include('layouts.pagination', ['dataType' => 'restaurantData'])
@endsection
