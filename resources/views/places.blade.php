@extends('layouts.app')

@section('Title','Places')

@section('css')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/dt-1.10.9/datatables.min.css"/>

@endsection


@section('content')

    <div class="col-md-9">

        <h1>Places in Tallahassee</h1>
        <hr>

        <p>

            Vim ea fugit phaedrum honestatis, ludus tantas constituam cu sed. Velit qualisque molestiae duo et. Et officiis ullamcorper pro, dictas vocent ex sit. Has id vivendo principes interpretaris, usu veniam aliquando ei. At mel referrentur definitionem, dicunt detracto mnesarchum duo cu. Dictas virtute vis id, autem sensibus ex sed.

            Et his justo dissentiet accommodare. Vel ne aperiam praesent. Tota explicari ullamcorper sit te, mandamus inimicus torquatos ex eam, oratio tantas offendit in quo. At pri tation discere adversarium, ubique mentitum ei mei. Usu no falli nonumes, id illud habemus apeirian mea. Dolores albucius intellegebat mel an, no eligendi quaestio sit. Facer disputando repudiandae vim ex.

            Et mea case quodsi.

        </p>

        <p>
            Duis dicat officiis pro at, ne probo utinam timeam est, suas deterruisset vix ne. Recteque abhorreant qui no, sea no recusabo gloriatur. Vix et doctus lucilius legendos, magna iusto pericula ex ius, mel zril discere officiis ex.

            Dicunt maluisset ex has. Simul solet epicurei mei at, purto legere populo te per. Pri ne iuvaret debitis, malis doming te has, congue soleat comprehensam duo ea. Molestie evertitur quaerendum vel et, sea postea similique scriptorem te. Dolorem feugait vivendo pri ad, ut eum euismod expetenda theophrastus. Cu cibo prompta vituperata mel, illum voluptaria vim no. No qui case persecuti, eum ex solet aeterno, his cu sumo persecuti.

            Legendos rationibus an eam, ne his mazim putent rationibus. Malis euripidis disputando at est, est te tempor nostrum. Est ut hinc legimus, inani cotidieque in vix, ne posse inermis interpretaris nam. Ius nonumes tincidunt no. Eos cu soluta aliquam senserit.

        </p>

        <table class="table table-bordered table-striped" id="table">
            <thead>
            <tr>
                <th>Place</th>
                <th>Address</th>

            </tr>
            </thead>
            <tbody>

            </tbody>

        </table>

    </div>

    <div class="col-md-3">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Tally -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-8386994709092971"
             data-ad-slot="6043619444"
             data-ad-format="auto"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});

        </script>
    </div>


@endsection


@section('javascript')

    <script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#table').DataTable({
                "bLengthChange": false,
                "info":     false,
                bFilter: false,
                "lengthMenu": [[5], [5, "All"]],
                bLength: 5
            });
        } );
    </script>

@endsection

@section('extend.footer')
    <p class="small">Map data is provided by Google and DigitalGlobe<br />
        Map data: <i class="fa fa-copyright"></i>{{date("Y")}} Google</p>
@endsection