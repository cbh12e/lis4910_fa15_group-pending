@extends('layouts.app')
@section('title','Bars')
@section('content')
    @include('adsense.header')

    <div class="row">
        <div class="col-md-12">
            <p class="lead">
               Tallahassee is a college town which means that there are plenty of bars to choose from.
                Here is the list of the bars located right in Tallahassee. </p>
        </div>
    </div>




    <div class="row">
            @include('table.table')
        </div>

    <div class="col-md-12">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Display Only Responsive -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-8386994709092971"
             data-ad-slot="8381526647"
             data-ad-format="auto"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>

@endsection

@section('javascript')
    @include('layouts.pagination', ['dataType' => 'barData'])
@endsection
