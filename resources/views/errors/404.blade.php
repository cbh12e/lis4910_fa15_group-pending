@extends('layouts.app')
@section('title','404 Error')

@section('content')

    <div class="col-md-9">
        <div class="jumbotron">
            <h1 class="text-center">404 Error</h1>
            <p class="lead text-center"> The page that you requested cannot be found! Please try your search again.</p>
        </div>
    </div>
@endsection