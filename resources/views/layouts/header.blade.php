<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="Whether you are a visitor, new student, transfer-student or a resident of Tallahassee or Leon County, there are a lot of things to do in Florida's Capitol City.
        This website was created to list the activities that anyone can do in Tallahassee.">
<meta name="author" content="">
<link rel="apple-touch-icon" sizes="57x57" href="{{asset('favicon/apple-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{asset('favicon/apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('favicon/apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('favicon/apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('favicon/apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{asset('favicon/apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{asset('favicon/apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{asset('favicon/apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicon/apple-icon-180x180.png')}}">
<link rel="apple-touch-icon" sizes="192x192" href="{{asset('favicon/apple-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('favicon/favicon-96x96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon/favicon-16x16.png')}}">
<link rel="manifest" href="{{asset('favicon/manifest.json')}}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{asset('favicon/ms-icon-144x144.png')}}">
<meta name="theme-color" content="#ffffff">

{{-- Use @section('header') to add more headers. Do not forget to close it off with @endsection--}}
@yield('header')

{{-- Use @section('title','Insert Text here') to override the default web title--}}
<title>@yield('title','Blank Title') | Places in Tallahassee</title>

<!-- Bootstrap core CSS -->
<link type="text/css" rel="stylesheet" href="{{cdn('css/bootstrap.css')}}" >

<!-- AdSense Responsive -->
<link type="text/css" rel="stylesheet" href="{{cdn('css/adsense.css')}}">

@section('custom-styles')
<!-- Custom CSS -->
<link type="text/css" rel="stylesheet" href="{{cdn('css/custom-styles.css')}}">
@show


        <!-- Font Awesome CDN -->
<link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Glyphicon CSS-->
{{-- Bootstrap 4.0 Alpha does not include Glyphicons in its CSS Stylesheet template --}}
<link type="text/css" rel="stylesheet" href="{{cdn('css/fonts.css')}}">

<!-- Google Analytics -->
<script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-51571203-3', 'auto');
    ga('send', 'pageview');
</script>