<!-- Placed at the end of the document so the pages load faster -->
<!-- JQuery 2.1.4 min-->
<script type="text/javascript" src="{{cdn('js/jquery.min.js')}}"></script>
<!-- Bootstrap 4.0 alpha -->
<script type="text/javascript" src="{{cdn('js/bootstrap.js')}}"></script>


{{-- use @section('javascript') to insert additional .js files and scripts--}}
@yield('javascript')

{{-- Don't forget to close this off with @endsection or the page wont load in a web page blade. --}}
