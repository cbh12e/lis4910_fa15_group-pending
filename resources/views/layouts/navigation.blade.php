{{-- Feel free to modify - Casey Hargarther --}}
<nav class="navbar navbar-dark bg-primary navbar-fixed-top">
    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#navbar">
        &#9776;
    </button>
    <div class="collapse navbar-toggleable-xs" id="navbar">
        <a class="navbar-brand" href="{{asset('home')}}" id="top">Places in Tallahassee</a>
        <ul class="nav navbar-nav">
            <li class="nav-item {{active('home')}} {{active('/')}}">
                <a class="nav-link" href="{{asset('home')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
               <!-- <a class="nav-link" href="#">Features</a>-->
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Places</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{asset('bars')}}"><i class="fa fa-beer"></i> Bars</a>
                    <a class="dropdown-item" href="{{asset('restaurants')}}"><i class="fa fa-cutlery"></i> Restaurants</a>
                    <a class="dropdown-item" href="{{asset('nightlife')}}"><i class="fa fa-users"></i> Nightlife</a>
                    <a class="dropdown-item" href="{{asset('theaters')}}"><i class="fa fa-film"></i> Movie Theaters</a>
                    <a class="dropdown-item" href="{{asset('parks')}}"><i class="fa fa-tree"></i> Parks</a>
                </div>
            </li>

            <li class="nav-item {{active('fsu')}} {{active('famu')}} dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Universities</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item {{active('fsu')}}" href="{{asset('fsu')}}"><i class="fa fa-university"></i> Florida State University</a>
                    <a class="dropdown-item {{active('famu')}}" href="{{asset('famu')}}"><i class="fa fa-university"></i> Florida Agricultural and Mechanical University</a>
                </div>
            </li>
           <!-- <li class="nav-item">
                <a class="nav-link" href="#">Blank</a>
            </li>-->
            <li class="nav-item {{active('about')}}">
                <a class="nav-link" href="{{asset('about')}}">About Tallahassee</a>
            </li>
        </ul>
    </div>
</nav>