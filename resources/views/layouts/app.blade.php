<!DOCTYPE html>
<html lang="en">
<head>
    {{-- Blade template is located in resources/views/layouts/header.blade.php --}}
@include('layouts.header')
    @yield('css'){{-- use @section('css') to insert additional css files and overrides --}}
</head>
<body>
{{-- Navigation Bar Blade is located in resources/views/layouts/navigation.blade.php --}}
@include('layouts.navigation')
@yield('frame')

<div class="container">
    {{-- Page Content use @section('content') in an blade template to add data into content --}}
    @yield('content')
</div> <!-- /container -->

@yield('fluid')
{{-- Footer Blade template located in resources/views/layouts/footer.blade.php --}}
@include('layouts.footer')
{{-- JavaScript script template is located in resources/views/layouts/footer.blade.php --}}
@include('layouts.javascript')
{{-- Lastly, Do not forget to close off the section page with @endsection in an blade template page --}}
</body>
</html>