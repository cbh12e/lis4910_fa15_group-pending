<hr>
    <div class="container">
        <div class="row">
            <footer class="footer">
                <p class=" text-muted"> <i class="fa fa-copyright"></i>{{date("Y")}} Places in Tallahassee</p>
                <p class="pull-right"><a href="#top">Back to top</a></p>
                @yield('extend.footer') {{-- Used to add attributions--}}
            </footer>
        </div>
    </div>