{{-- Ajax for Pagination - Do not modify this file. --}}
<script type="text/javascript">
    $(document).on('click','.pagination a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        getProducts(page);
    });
    function getProducts(page)
    {
        $.ajax({
            url:'{{asset('')}}{!! $dataType !!}?page='+page
        })
                .done(function(data){
                    $('.ajaxContent').html(data);
                    location.hash = page;
                });
    }
</script>