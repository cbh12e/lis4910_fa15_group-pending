@extends('layouts.app')
@section('title','Movie Theaters')
@section('css')

    @endsection

@section('content')
    @include('adsense.header')
    <div class="row">
        <div class="col-md-12">
            <p class="lead">
                There are multiple movie theaters located right in the City of Tallahassee. There are two that run the newest released movies and two that do repeat releases.            </p>

            <p>
             There are new premiers playing on a regular basis and can be seen even in Tallahassee. If you are a student at Florida State University, you can see the movies in the Ruebin O. Askew Student Life Center conveniently located on campus across the street from Traditions Hall. These movies are free with a valid FSU Student ID card.</p>

        </div>
    </div>

    <div class="row">
    <div class="col-md-12 col-sm-12 col-lg-10 col-xl-10 col-xs-12">
            {{-- Located in the table directory resources/views/table/table.blade.php --}}
        @include('table.table')
        </div>
<div class="col-lg-2 col-xl-2">

    @include('adsense.sidebar2')
</div>

    </div>

@include('adsense.bottom')
       @endsection
@section('javascript')
    @include('layouts.pagination', ['dataType' => 'movieData'])
@endsection
@section('extend.footer')
    <p class="small">Map data is provided by Google and DigitalGlobe<br />
        Map data: <i class="fa fa-copyright"></i>{{date("Y")}} Google</p>
@endsection