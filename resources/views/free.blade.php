@extends('layouts.app')
@section('title','Free')
@section('content')
    <div class="col-md-12">
        <!-- Page Header -->
        <h1>Free in Tallahassee</h1>
        <hr>
        {{-- Paragraph Text --}}
        <p>Tallahassee has a lot of free options to choose from. .................</p>
        {{-- Thumbnail data Blade - This file is located in the resources/views/table directory. This file is called free.blade.php --}}
        @include('table.free')
    </div>
@endsection
@section('javascript')
    @include('layouts.pagination', ['dataType' => 'freeData'])
@endsection

@section('extend.footer')
    <p class="small">Map data is provided by Google and DigitalGlobe<br />
        Map data: <i class="fa fa-copyright"></i>{{date("Y")}} Google</p>
@endsection