@extends('layouts.app')
@section('title','Home')
@section('content')
        <!-- Main component for a primary marketing message or call to action -->
<div class="row">
    <div class="col-md-12">
        <div class="card home-card">
            <img class="img-thumbnail" src="{{cdn('images/tally.jpg')}}" alt="Tallahassee"/>
            <div class="card-block">
                <h1 class="card-title">Tallahassee, Florida</h1>
                <p class="card-text"> Whether you are a visitor, new student, transfer-student or a resident of Tallahassee or Leon County, there are a lot of things to do in Florida's Capitol City.
                    This website was created to list the activities that anyone can do in Tallahassee.</p>
            </div>
        </div>
    </div>
</div>

<!-- Example row of columns -->
        <div class="row">
            <div class="col-lg-4">
                <div class="card card-block home-card">
                    <h2 class="card-title">Free Activities</h2>
                    <p class="card-text">There is lots of free activities to do in Tallahassee. Here is the list of free activities within Tallahassee.</p>
                   <a class="btn btn-primary" href="{{asset('free')}}" role="button"><i class="fa fa-circle"></i> Free &raquo;</a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-block home-card">
                    <h2 class="card-title">Places to Eat</h2>
                    <p class="card-text">Tallahassee is home to a number of restaurants. Here is a list of select affordable restaurants.</p>
                    <a class="btn btn-primary" href="{{asset('restaurants')}}" role="button"><i class="fa fa-cutlery"></i> Restaurants &raquo;</a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-block home-card">
                    <h2 class="card-title">Parks</h2>
                    <p class="card-text">Tallahassee is home to a number of parks. Here is the list of parks that are located within Tallahassee. </p>
                    <a class="btn btn-primary" href="{{asset('parks')}}" role="button"><i class="fa fa-tree"></i> Parks &raquo;</a>
                </div>
            </div>
        </div>

<div class="col-md-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Display Only Responsive -->
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-client="ca-pub-8386994709092971"
         data-ad-slot="8381526647"
         data-ad-format="auto"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>

@endsection