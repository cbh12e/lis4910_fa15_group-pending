<div class="ajaxContent">
    <div class="card-deck-wrapper">
        <div class="card-deck">
            @foreach($place as $places)
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">{{$places->park_name}}</h4>
                        <h6 class="card-subtitle text-muted"></h6>
                    </div>
                    @if($places->park_lng !== null)
                        <img src="{{staticMap($places->park_lng, $places->park_lat,$places->park_zoom)}}" class="img-responsive card-img" style="height: 300px; width: 100%; display: block;" alt="{{$places->park_name}}">
                    @else
                        <img src="https://placehold.it/220x200" class="img-responsive card-img" style="height: 180px; width: 100%; display: block;">
                    @endif
                    <div class="card-block">
                        <p class="card-text">
                            {{$places->park_street}}<br/>
                            {{$places->park_city}}, {{$places->park_state}} {{$places->park_zip}}
                        </p>
                        @if($places->park_directions !== null)
                            <a href="{{$places->park_directions}}" target="_blank" class="card-link">Get Directions</a>
                        @endif
                        @if($places->park_website !== null)
                            <a href="{{$places->park_website}}" target="_blank" class="card-link">Visit Website</a>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="text-right">
         <!-- Laravel Paginate -->
         <!-- This is separate from the DataTables Plugin as this is not using a table entry-->
         {!! $place->render() !!}
    </div>
</div>