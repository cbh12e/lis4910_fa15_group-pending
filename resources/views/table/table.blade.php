<div class="ajaxContent">
    <div class="card-deck-wrapper">
        <div class="card-deck">
            @foreach($place as $places)
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">{{$places->pl_name}}</h4>
                        <h6 class="card-subtitle text-muted">@if($places->pl_phone !== null)<i class="fa fa-phone"></i> {{phone($places->pl_phone)}}@endif</h6>
                    </div>
                    @if($places->pl_lng !== null)
                        <img src="{!!staticMap($places->pl_lng,$places->pl_lat,$places->pl_zoom)!!}" class="img-fluid card-img" alt="{{$places->pl_name}}" style="height: 200px; width: 100%; display: block;">
                    @else
                        <img src="https://placehold.it/640x400" class="img-fluid card-img" alt="{{$places->pl_name}}" style="height: 180px; width: 100%; display: block;">
                    @endif
                    <div class="card-block">
                        <p class="card-text"> {{$places->pl_address}} <br />{{$places->pl_city}}, {{$places->pl_state}} {{$places->pl_zip}}</p>
                        @if($places->pl_web !== null)
                            <a href="{{$places->pl_web}}" target="_blank" class="card-link">Visit Website</a>
                        @endif
                        @if($places->pl_directions !== null)
                            <a href="{{$places->pl_directions}}" target="_blank" class="card-link">Directions</a>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="text-right">
            <!-- Laravel Paginate -->
            {!! $place->render() !!}
        </div>
    </div>
</div>