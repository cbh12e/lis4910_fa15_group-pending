<div class="ajaxContent">
    <div class="card-deck-wrapper">
        <div class="card-deck">
        @foreach($place as $places)
                <div class="card seminole">
                    <div class="card-block">
                        <h4 class="card-title">{{$places->pl_name}}</h4>
                        <h6 class="card-subtitle text-muted">{{$places->pl_type}}</h6>
                    </div>
                    @if($places->pl_image !== null)
                    <img src="{{$places->pl_image}}" class="img-responsive card-img" alt="{{$places->pl_name}}" style="height: 180px; width: 100%; display: block;">
                    @else
                        <img src="https://placehold.it/220x200" class="img-responsive card-img" alt="{{$places->pl_name}}" style="height: 200px; width: 100%; display: block;">
                    @endif
                        <div class="card-block">
                        <p class="card-text"> {{$places->pl_address}} <br />{{$places->pl_city}}, {{$places->pl_state}} {{$places->pl_zip}}
                            <br /> <i class="fa fa-phone"></i> {{phone($places->pl_phone)}}</p>
                            @if($places->park_directions !== null)
                                <a href="{{$places->park_directions}}" target="_blank" class="card-link">Get Directions</a>
                            @endif
                            @if($places->park_web !== null)
                                <a href="{{$places->park_web}}" target="_blank" class="card-link">Visit Website</a>
                            @endif
                    </div>
                </div>
        @endforeach
    </div>
    </div>
    <div class="row">
        <div class="text-right">
            <!-- Laravel Paginate -->
            {!! $place->render() !!}
        </div>
    </div>
</div>