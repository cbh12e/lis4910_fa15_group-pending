<div class="ajaxContent">
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Name:</th>
        <th>Address:</th>
        <th>Phone:</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($place as $places)
        <tr>
            <td>{{$places->pl_name}}</td>
            <td>{{$places->pl_address}} <br />{{$places->pl_city}}, {{$places->pl_state}} {{$places->pl_zip}}</td>
            <td><i class="fa fa-phone"></i> {{phone($places->pl_phone)}}</td>
            <td>
                @if($places->pl_web)
                    <a href="{!!$places->pl_web!!}" class="btn btn-primary"><i class="fa fa-globe"></i> Website</a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-right">
    <!-- Laravel Paginate -->
    {!! $place->render() !!}
</div>
</div>