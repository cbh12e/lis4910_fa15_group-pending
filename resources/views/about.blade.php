@extends('layouts.app')

@section('title','About Tallahassee')

@section('css')
    <style>
        #map {
            max-width: 520px;
            height: 300px;
            width:100%;

        }
    </style>
@endsection


@section('content')

<h1> About Tallahassee</h1>
<hr>
<div class="col-md-6">

    <p class="lead">Tallahassee, is well known as being the capital of the State of Florida and a classic college town.
        Tallahassee not only serves as Florida's Capitol City, it is also home to Florida State University, Florida Agricultural and Mechanical University, and Tallahassee Community College.
        Tallahassee is a diverse city with several parks, different types of restaurants and has lots of fun activities to do. Interesting facts about Tallahassee is that it was founded in 1821, located between St. Augustine and Pensacola and was the only Confederate state capitol not to be captured by Union Forces east of the Mississippi River during the Civil War.
</div>

<div class="col-md-6">

    <!-- Google Maps API -->
    <div id="map"></div>

</div>

@endsection

@section('javascript')
    <!-- Google Maps JavaScript API -->
    <script type="text/javascript">
        function initMap() {
            var customMapType = new google.maps.StyledMapType([
                {
                    stylers: [
                        {hue: '#CEB888'},
                        {visibility: 'simplified'},
                        {gamma: 0.5},
                        {weight: 0.5}
                    ]
                },
                {
                    elementType: 'labels',
                    stylers: [{visibility: 'on'}]
                },
                {
                    featureType: 'water',
                    stylers: [{color: '#0275d8'}]
                }
            ], {
                name: 'Map'
            });
            var customMapTypeId = 'custom_style';

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: {lat: 30.433, lng: -84.294},  // Tallahassee
                mapTypeControlOptions: {
                    mapTypeIds: [customMapTypeId]
                }
            });

            map.mapTypes.set(customMapTypeId, customMapType);
            map.setMapTypeId(customMapTypeId);
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&signed_in=true&callback=initMap"
            async defer></script>


@endsection

@section('extend.footer')
    <p class="small">Map data is provided by Google and DigitalGlobe<br />
        Map data: <i class="fa fa-copyright"></i>{{date("Y")}} Google</p>
@endsection