<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Parks
 *
 * @package App
 */
class Parks extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'parks';

    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['park_name', 'park_street','park_city','par_state','park_zip','park_phone','park_notes'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
}
