<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Place
 *
 * @package App
 */
class Place extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pl_name', 'pl_type', 'pl_street','pl_city','pl_state','pl_zip','pl_phone','pl_notes','pl_image'];

    /**
     * The attributes which may be used for sorting dynamically.
     *
     * @var array
     */
    protected $sortable = ['pl_name', 'pl_id', 'pl_type'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
}
