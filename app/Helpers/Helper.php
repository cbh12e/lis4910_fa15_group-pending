<?php

/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 11/6/2015
 * Time: 7:11 PM
 */

/**
 * Returns default string 'active' if route is active.
 *
 * @param $route
 * @param string $str
 * @return string
 */
function active($route, $str = 'active') {

    return call_user_func_array('Request::is', (array)$route) ? $str : '';

}

function phone($data)
{

        // add logic to correctly format number here
        // a more robust ways would be to use a regular expression
        return "(".substr($data, 0, 3).") ".substr($data, 3, 3)."-".substr($data,6);
    //return "";
}

function staticMap($lon, $lat, $zoom)
{

    $api = "https://maps.googleapis.com/maps/api/staticmap?center=".$lon.",".$lat."&zoom=".$zoom."&size=640x400&style=element:geometry.stroke|visibility:on&style=feature:landscape|element:geometry|saturation:-100&style=feature:water|saturation:0|invert_lightness:true&key=".env('GOOGLE_API');


    return $api;
}

// global CDN link helper function
function cdn( $asset ){

    // Verify if KeyCDN URLs are present in the config file
    if( !Config::get('app.cdn') )
        return asset( $asset );

    // Get file name incl extension and CDN URLs
    $cdns = Config::get('app.cdn');
    $assetName = basename( $asset );

    // Remove query string
    $assetName = explode("?", $assetName);
    $assetName = $assetName[0];

    // Select the CDN URL based on the extension
    foreach( $cdns as $cdn => $types ) {
        if( preg_match('/^.*\.(' . $types . ')$/i', $assetName) )
            return cdnPath($cdn, $asset);
    }

    // In case of no match use the last in the array
    end($cdns);
    return cdnPath( key( $cdns ) , $asset);

}

function cdnPath($cdn, $asset) {
    return  "//" . rtrim($cdn, "/") . "/" . ltrim( $asset, "/");
}