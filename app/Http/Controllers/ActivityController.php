<?php

namespace App\Http\Controllers;

use App\Parks;
use App\Place;
use App\Http\Requests;
use Illuminate\Database\Eloquent;

/**
 * Class ActivityController
 *
 * @package App\Http\Controllers
 */
class ActivityController extends Controller
{
    /**
     * @var $place
     */
    protected $place;

    /**
     * List Restaurants in Tallahassee
     */
    public function restaurant()
    {
        $place = Place::where('pl_type','Restaurant')
            ->where('pl_college','LIKE', '%1%')
            ->orderBy('pl_name')
            ->paginate(10);

     return view('restaurants',compact('place'));

    }

    /**
     * @return string
     */
    public function listRestaurant()
    {
        $place = Place::where('pl_type', 'Restaurant')
            ->where('pl_college','LIKE', '%1%')
            ->orderBy('pl_name')
            ->paginate(10);

        return view('table.restaurant', compact('place'))->render();

    }

    /**!
     * List Bars in Tallahassee
     */
    public function bars()
    {
        $place = Place::where('pl_type','Bar')
            ->orderBy('pl_name')
            ->paginate(3);

     return view('bars',compact('place'));
    }

    /**
     * @return string
     */
    public function listBars()
    {
       $place = Place::where('pl_type', 'Bar')
           ->orderBy('pl_name')
           ->paginate(3);

       return view('table.table', compact('place'))->render();
    }

    /**!
     * Nightlife List in Tallahassee
     */
    public function nightLife()
    {
        $place = Place::where('pl_type','Nightlife')
            ->orderBy('pl_name')
            ->paginate(3);

     return view('nightlife',compact('place'));
    }

    /**
     * @return string
     */
    public function listNight()
    {
       $place = Place::where('pl_type', 'Nightlife')
           ->orderBy('pl_name')
           ->paginate(3);

       return view('table.table', compact('place'))->render();
    }

    /**
     * List Movie Theaters in Tallahassee
     */

    public function movies()
    {
        $place = Place::where('pl_type','Movie Theater')
            ->orderBy('pl_name')
            ->paginate(4);

     return view('movies',compact('place'));

    }

    /**
     * @return string
     */
    public function listMovies()
    {
        $place = Place::where('pl_type', 'Movie Theater')
            ->orderBy('pl_name')
            ->paginate(4);

        return view('table.table', compact('place'))->render();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function free()
    {
        $place = Place::where('pl_free','Y')
            ->paginate(3);

     return view('free',compact('place'));

    }

    /**
     * @listFree
     *
     * @return string
     */
    public function listFree()
    {
        $place = Place::where('pl_free','Y')
            ->paginate(3);

     return view('table.free',compact('place'))->render();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function park()
    {
        $place= Parks::orderBy('park_name')
            ->paginate(3);

        return view('park', compact('place'));
    }

    /**
     * @return string
     */
    public function listParks()
    {
        $place= Parks::orderBy('park_name')
            ->paginate(3);

        return view('table.park', compact('place'))->render();

    }
}