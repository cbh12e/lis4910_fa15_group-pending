<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 11/24/2015
 * Time: 2:06 PM
 */

namespace App\Http\Controllers;

use App\Place;
use App\Http\Requests;
use Illuminate\Database\Eloquent;

/**
 * Class CollegeController
 *
 * @package App\Http\Controllers
 */
class CollegeController extends Controller
{

    /**!
     * List FSU Venues
     */
    public function fsu()
    {
        $place = Place::where('pl_type','FSU')
            ->orderBy('pl_name')
            ->paginate(3);

        return view('college.fsu',compact('place'));
    }

    /**
     * @return string
     */
    public function listFsu()
    {
        $place = Place::where('pl_type', 'FSU')
            ->orderBy('pl_name')
            ->paginate(3);

        return view('table.table', compact('place'))->render();
    }

    /**!
     * List FAMU Venues
     */
    public function famu()
    {
        $place = Place::where('pl_type','FAMU')
            ->orderBy('pl_name')
            ->paginate(3);

        return view('college.famu',compact('place'));
    }

    /**
     * @return string
     */
    public function listFamu()
    {
        $place = Place::where('pl_type', 'FAMU')
            ->orderBy('pl_name')
            ->paginate(3);
        return view('table.table', compact('place'))->render();
    }



    /**
     * List Restaurants in Tallahassee
     */
    public function restaurantFamu()
    {
        $place = Place::where('pl_type','Restaurant')
            ->where('pl_college','LIKE', '%2%')
            ->orderBy('pl_name')
            ->paginate(5);

        return view('college.food-famu',compact('place'));

    }

    /**
     * @return string
     */
    public function listRestaurantFamu()
    {
        $place = Place::where('pl_type', 'Restaurant')
            ->where('pl_college','LIKE', '%2%')
            ->orderBy('pl_name')
            ->paginate(5);

        return view('table.restaurant', compact('place'))->render();

    }


}