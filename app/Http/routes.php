<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('{path}', function($filename) {
    return Bust::css($filename);
})->where('path', '.*\.css$');
App::make('cachebuster.StripSessionCookiesFilter')->addPattern('|\.css$|');

Route::get('/', function () {
    return view('home');
});

Route::get('home', function () {
    return view('home');
});

Route::get('places', function () {
    return view('places');
});

Route::get('activities', function () {
    return view('activities');
});


Route::get('about', function () {
    return view('about');
});

/**
 * Returns the links of universities located in the City of Tallahassee.
 */

Route::get('fsu', 'CollegeController@fsu');
Route::get('fsuData', 'CollegeController@listFsu');

Route::get('famu', 'CollegeController@famu');
Route::get('famuData', 'CollegeController@listFamu');

Route::get('famu/restaurants', 'CollegeController@restaurantFamu');
Route::get('famu/restaurantData', 'CollegeController@ListRestaurantFamu');

/**
 * This is the Controller that takes the user to the page that lists the cards.
 */

Route::get('restaurants', 'ActivityController@restaurant');
Route::get('restaurantData', 'ActivityController@ListRestaurant');

Route::get('bars', 'ActivityController@bars');
Route::get('barData', 'ActivityController@listBars');

Route::get('nightlife', 'ActivityController@nightLife');
Route::get('nightData', 'ActivityController@listNight');

Route::get('theaters', 'ActivityController@movies');
Route::get('movieData', 'ActivityController@listMovies');

Route::get('parks', 'ActivityController@park');
Route::get('parkData', 'ActivityController@listParks');

Route::get('free', 'ActivityController@free');
Route::get('freeData', 'ActivityController@listFree');


