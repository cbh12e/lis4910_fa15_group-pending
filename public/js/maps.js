/**
 * Created by hargcb on 12/8/2015.
 */

function initMap() {

    var customMapType = new google.maps.StyledMapType([
        {
            stylers: [
                {hue: '#CEB888'},
                {visibility: 'simplified'},
                {gamma: 0.5},
                {weight: 0.5}
            ]
        },
        {
            elementType: 'labels',
            stylers: [{visibility: 'on'}]
        },
        {
            featureType: 'water',
            stylers: [{color: '#0275d8'}]
        }
    ], {
        name: 'Map'
    });
    var customMapTypeId = 'custom_style';

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 30.433, lng: -84.294},  // Tallahassee
        mapTypeControlOptions: {
            mapTypeIds: [customMapTypeId]
        }
    });

    map.mapTypes.set(customMapTypeId, customMapType);
    map.setMapTypeId(customMapTypeId);
}