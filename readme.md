## Team Pending LIS4910-02: Information Technology Project - Fall Semester 2015

This website is built using the Laravel 5.1 Framework.

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

To get all the required files after cloning this repository, you will need to enter database information and run the migration file. First you need to enter *composer install* in Terminal or Command Prompt to get this repository to download all the required files in order to view the website on your local machine.

Webpages made with Laravel use blade templates, which are located in the resources/views folder. External files like Photos, screenshots, JavaScript, CSS files and equivalent files are located in the public folder located in the root directory.


## Bootstrap 4 Alpha 

This website uses the Alpha version of the Bootstrap 4, you will need to refer to this link to refer for documentation. 
http://v4-alpha.getbootstrap.com/components/

There is also classes in Bootstrap 3 that does not work with Bootstrap 4:

Most Common:

Thumbnails .thumbnail-> use .cards

Panels .panel-> use .cards

Wells .wells -> use .cards

Carousel Item .item ->  use .carousel-item

Page Header .page-header -> Not Supported in Bootstrap 4

Default + Info buttons .btn-default and .btn-info  -> use .btn-secondary instead

Extra Small Button .btn-xs -> Not Supported in Bootstrap 4

Form Horizontal .form-horizontal -> use .row instead

Navigation -> new classes added, see documentation links below for more details.


Refer to this to find out the differences: http://v4-alpha.getbootstrap.com/migration/ or this for the side by side differences: http://www.quackit.com/bootstrap/bootstrap_4/differences_between_bootstrap_3_and_bootstrap_4.cfm


## Local Installation

Once the repository has been successfully cloned from BitBucket, you will need to run *composer install* to install all the required vendor files to run Laravel.

You will need to create a new file in the root directory called .**env** with clicking Control + A or Command +A in the **.env.example** file to copy the data and then pasting it in the new file. Do not rename or delete this file!

Enter your database name and login credentials if your database credentials on your local machine is different than the one located on the **.env** file.

You will need to create a database using either phpMyAdmin, Terminal, Command Prompt, Toad SQL, HediSQL and/or equivalent. This database must be empty with no tables or data in order to use the php artisan migrate command or the migration will fail.

In order to get the app to read MySQL data, you will need to run the command "*php artisan migrate:install*".

If this directory is in a sub-directory called whatever in the root directory then to access the page, you will need to try either http://localhost/whatever/public/home OR http://localhost/whatever/index.php/public/home. FYI common mistakes are to type in http://localhost/whatever or http://localhost/whatever/index.php. However, both of these examples will either return you either a index page of the public directory or an 403 Forbidden message in a more secure installation.

## Contributing

Casey Hargarther

Samuel Alwine

Elston Brown

Daniel Rouse

## Requirements

A computer with a LAMP, WAMP, or MAMP web server installation.

Windows Vista, 7, 8 or 10, OS X, or Linux based operating system

Apache Web Server

PHP 5.5 and up

MySQL or MariaDB with valid access credentials and permissions

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)


## Landing Page Screenshot

![Alt text](/screenshots/screenshot.png?raw=true "Landing Page Screenshot")
![Alt text](/screenshots/screenshot6.png?raw=true "Bar Page Screenshot")
![Alt text](/screenshots/screenshot3.png?raw=true "Park Page Screenshot")
![Alt text](/screenshots/screenshot5.png?raw=true "Movie Page Screenshot")
![Alt text](/screenshots/screenshot4.png?raw=true "Meal Page Screenshot")