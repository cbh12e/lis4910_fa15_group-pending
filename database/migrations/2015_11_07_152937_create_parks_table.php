<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateParksTable
 */
class CreateParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('park_name',255);
            $table->string('park_street', 100);
            $table->string('park_city', 30)->default('Tallahassee');
            $table->char('park_state',10)->default('Florida');
            $table->char('park_zip',5)->default('32300');
            $table->char('park_phone',10)->nullable();
            $table->string('park_website',300)->nullable();
            $table->string('park_directions',600)->nullable();
            $table->string('park_photo',300)->nullable();
            $table->string('park_notes',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('parks');

    }
}
