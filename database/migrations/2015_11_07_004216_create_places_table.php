<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePlacesTable
 */
class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pl_name',255);
            $table->string('pl_logo')->nullable();
            $table->enum('pl_type', ['Restaurant','Mall','School','Food','Shopping','Library','Movie Theater','Entertainment']);
            $table->enum('pl_free', ['Y','N'])->default('N');
            $table->string('pl_address', 100);
            $table->string('pl_city', 30)->default('Tallahassee');
            $table->char('pl_state',2)->default('FL');
            $table->char('pl_zip',5)->default('32300');
            $table->char('pl_phone',10)->nullable();
            $table->string('pl_photo',300)->nullable();
            $table->string('pl_web',300)->nullable();
            $table->string('pl_directions',600)->nullable();
            $table->string('pl_notes',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('places');
    }
}